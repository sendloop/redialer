// Package smtpredialer provides a redialer for smtp.Client.
package smtpredialer

import (
	"crypto/tls"
	"io"
	"log"
	"net"
	"net/smtp"
	"net/url"

	"bitbucket.org/sendloop/redialer"
)

type smtpDialer struct {
	Address string
}

func (d smtpDialer) Dial() (io.Closer, error) {

	u, err := url.Parse(d.Address)
	if err != nil {
		return nil, err
	}

	user := u.User.Username()
	pass, _ := u.User.Password()
	host, _, _ := net.SplitHostPort(u.Host)

	conn, err := smtp.Dial(u.Host)

	tlsEnabled := false
	m, _ := url.ParseQuery(u.RawQuery)
	if val, ok := m["tls"]; ok {
		if val[0] == "true" {
			tlsconfig := &tls.Config{
				InsecureSkipVerify: false,
				ServerName:         host,
			}
			if err := conn.StartTLS(tlsconfig); err != nil {
				log.Fatal(err)
			}
			tlsEnabled = true
		}
	}

	var auth smtp.Auth
	if len(user) > 0 && len(pass) > 0 {
		if tlsEnabled {
			auth = smtp.PlainAuth("", user, pass, host)
		} else {
			auth = smtp.CRAMMD5Auth(user, pass)
		}
	}

	if auth != nil {
		if err := conn.Auth(auth); err != nil {
			log.Fatal(err)
		}
	}

	return conn, err
}

func (d smtpDialer) Addr() string {
	return d.Address
}

func (d smtpDialer) OnConnect(conn *redialer.Conn) error {
	return nil
}

type SMTPRedialer struct {
	redialer *redialer.Redialer
}

func New(address string) *SMTPRedialer {
	d := &smtpDialer{address}
	return &SMTPRedialer{redialer.New(d)}
}

func (r *SMTPRedialer) Run() {
	r.redialer.Run()
}

func (r *SMTPRedialer) Close() error {
	return r.redialer.Close()
}

func (r *SMTPRedialer) Client() <-chan *Client {
	ch := make(chan *Client, 1)
	go r.notifyConn(ch)
	return ch
}

func (r *SMTPRedialer) notifyConn(ch chan<- *Client) {
	rconn, ok := <-r.redialer.Conn()
	if !ok {
		close(ch)
		return
	}
	client := rconn.Get().(*smtp.Client)
	ch <- &Client{client, rconn}
}

type Client struct {
	*smtp.Client
	rconn *redialer.Conn
}

func (c *Client) Close() error {
	err := c.Client.Close()
	c.rconn.SetClosed()
	return err
}
